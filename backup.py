# from datetime import datetime
import os
import time  #buat time sleep agar ada jeda beberapa detik di program
from prettytable import PrettyTable
os.system('cls')
import datetime


rute = [{
    "asal" : "merak",
    "tujuan" : "Balikpapan",
    "tarif" : 100000,
    "tanggal" : "12-05-2022",
    "jam" : "12:30"},
    {
    "asal" : "samarinda",
    "tujuan" : "Surabaya",
    "tarif" : 170000,
    "tanggal" : "22-06-2022",
    "jam" : "12:30"},
      {
    "asal" : "samarinda",
    "tujuan" : "sulawesi",
    "tarif" : 200000,
    "tanggal" : "22-06-2022",
    "jam" : "12:30"},
      {
    "asal" : "pare pare",
    "tujuan" : "Surabaya",
    "tarif" : 340000,
    "tanggal" : "22-06-2022",
    "jam" : "12:30"},
      {
    "asal" : "kep. seribu",
    "tujuan" : "makassar",
    "tarif" : 120000,
    "tanggal" : "22-06-2022",
    "jam" : "12:30"},
      {
    "asal" : "jakarta",
    "tujuan" : "aceh",
    "tarif" : 230000,
    "tanggal" : "22-06-2022",
    "jam" : "12:30"},
      {
    "asal" : "singapur",
    "tujuan" : "balikpapan",
    "tarif" : 700000,
    "tanggal" : "22-06-2022",
    "jam" : "12:30"},
      {
    "asal" : "batam",
    "tujuan" : "sumatra",
    "tarif" : 900000,
    "tanggal" : "22-06-2022",
    "jam" : "12:30"}
]

biodata = []
us_admin =("admin")
pw_admin =("admin")
daftarnama = [] #pengganti user=['admin]

tabel1=PrettyTable()
tabel1.field_names=["NO","Pelabuhan Awal","Pelabuhan Tujuan","Tarif","tgl berangkat","Jam"]

tabel2 = PrettyTable()
tabel2.field_names = ['Pelabuhan Asal', 'Pelabuhan Tujuan', 'Jumlah Tiket', 'Total']

tabel3 = PrettyTable()
tabel3.field_names = ["NO","Pelabuhan Awal","Pelabuhan Tujuan","Tarif","tgl berangkat","Jam"]

def tabel_data():
    tabel1.clear_rows()
    for i in range(len(rute)):
        angka = rute[i]['tarif']
        format = "{:,}".format(angka)
        tabel1.add_row([i+1, rute[i]['asal'], rute[i]['tujuan'], f"RP.{format}",rute[i]["tanggal"],rute[i]["jam"]] )

def tabel_pesan(data):
    tabel2.clear_rows()
    for i in range(len(data)):
        angka = data[i][3]
        format = "{:,}".format(angka)
        tabel2.add_row([data[i][0],data[i][1],data[i][2],f"RP.{format}"])

def tabel_data_tertentu(key):
    tabel3.clear_rows()
    if key in range(len(rute)):
        angka = rute[key]['tarif']
        format = "{:,}".format(angka)
        tabel3.add_row([key+1,rute[key]['asal'], rute[key]['tujuan'], f"RP.{format}",rute[key]["tanggal"],rute[key]["jam"]])
def menu_user(namas, data):    
    date_time = datetime.datetime.now()
    while True:
        os.system("cls")
        print()
        print(f"""
 Selamat datang {namas} di pemesanan tiket kapal        
+---------------------------------------+       
     waktu lokal : {date_time.strftime("%a %x %H:%M")} 
+---------------------------------------+
        ========================       
        |       Menu User      |
        |----------------------|       
        | 1. Pesan tiket       |
        | 2. Lihat Pesanan     |
        | 0. Halaman Login     |
        ========================   
            """)
        casea = input("MASUKAN PILIHAN MENU [1/2/0]: ")
        if casea == '1' or casea == 'pesan':
            user_pesan(namas, data)
        
        elif casea == '2' or casea == 'lihat':
            user_data(namas, data)
            
        elif casea == '0' or casea == 'login':
            menulogin()
def user_data(namas, data):
    if len(data) <= 0:
        print("\n>>>>>> Anda belum memesan tiket <<<<<<")
    else:
        os.system('cls')
        print("==========================================")
        print("Halo",namas ,"Anda memesan tiket : ")
        tabel_pesan(data)    
        print(tabel2)

        total = 0
        for i in data:
            total = total + i[3]
            angka = total
            format = "{:,}".format(angka)
        print(f"\nTotal yang harus dibayar adalah: Rp.{format}")
        print("==========================================")
        print()
    input('Tekan ENTER untuk melanjutkan......')
def user_pesan(namas, data):
    while True:
        os.system('cls')
        tabel_data()
        print(tabel1)
        while True:
            try:
                print("masukan angka 0 untuk kembali ke-menu user")
                casebb = int(input("Masukan No Pemesanan : "))-1
                if casebb == -1 :
                    menu_user(namas, data)
                else:    
                    break
            except ValueError:
                print("Data Tidak ditemukan")
                time.sleep(1)
                continue
        if casebb in range(len(rute)):
            while True:
                try:
                    berapa = int(input("Ingin pesan berapa tiket ? : "))
                    if berapa <1 :
                        print("anda tidak membeli apapun, periksa kembali pesanan anda")
                        time.sleep(2.5)                          
                        user_pesan(namas, data)
                    else:                   
                        angka = rute[casebb]['tarif']
                        format = "{:,}".format(angka)
                        print(f'\nAnda Memilih Rute {rute[casebb]["asal"]} - {rute[casebb]["tujuan"]} Dengan Harga Rp.{format} x {berapa} orang')
                        totals = rute[casebb]['tarif'] * berapa
                        data.append([rute[casebb]['asal'],rute[casebb]['tujuan'],berapa, totals])
                        tanya = input("\nApakah Anda Ingin Memesan Lagi ? (y/n) : ")
                        if tanya.lower() == 'y':
                            os.system('cls')
                            break
                        else :
                            print()
                            print('>>>>>>>>>>> Terimakasih Telah Memesan <<<<<<<<<<')
                            print()
                            time.sleep(2)
                            menu_user(namas, data)
                except ValueError:
                    continue
        else:
            continue
def menu_admin():
    while True:
        os.system("cls")
        print("""Hallo Admin\nSelamat Datang :)""")
        print("""
    Menu Admin     
====================
 [1.] Tambah Data    
 [2.] Cek Data       
 [3.] Ubah Data      
 [4.] Hapus Data     
 [0.] Halaman Login  
====================""")
        caseb = input("Pilih [1/2/3/4/0] : ")
        if caseb == '1':
            tambah_data()
        elif caseb == '2':
            cek_data()
        elif caseb == '3':
            ubah_data()
        elif caseb == '4':
            hapus_data()
        elif caseb == '0':
            menulogin()
# CREATE DATA
def tambah_data():
    while True:
        os.system("cls")
        print("""
Menu Ubah Data :
--------------------------------------
[1.] menambah data [rute/tarif/tanggal]
[0.] kembali ke Menu Utama
--------------------------------------""")
        menu1=(input("Pilih [1/0] : "))
        if menu1 == '1':
            while True:
                from datetime import datetime
                print()
                print("""
      SILAHKAN MASUKAN DATANYA
--------------------------------------
                    """)
                asal = input("Tambahkan Asal Keberangkatan : ").upper()
                tujuan = input("Tambahkan Tujuan Keberangkatan : ").upper()
                while True:
                    try:
                        tarif = int(input("Tambahkan Tarif Rute : "))
                        break
                    except ValueError:
                        print("TARIF HARUS BERUPA ANGKA!")
                        time.sleep(1.5)
                while True:
                    try:
                        inputan0 = input("Tambahkan tanggal (dd-mm-yyyy) cnth:(21-08-2002) : ")
                        format_data = datetime.strptime(inputan0,"%d-%m-%Y")
                        tgl = format_data.strftime(f"{'%d-%m-%Y'}")
                        break
                    except ValueError:
                        print("masukan format yg benar")
                        time.sleep(1.5)
                while True:        
                    try:
                        inputan1 = input("Tambahkan Jam (HH:MM) cnth: 20:30) : ")
                        format_data = datetime.strptime(inputan1,"%H:%M")
                        jam = format_data.strftime(f"{'%H:%M'}")
                        break
                    except ValueError:
                        print("masukan format yg benar")
                        time.sleep(1.5)
                    
                rute.append({'asal':asal,'tujuan':tujuan,'tarif':tarif,"tanggal":tgl,"jam":jam})
                print("sedang Menambahkan data --- ")
                time.sleep(1.5)
                print("Berhasil Menambahkan data --- ")
                time.sleep(2.5)
                break
        elif menu1 == '0' :
            os.system('cls')
            print(f"ke menu utama --- {time.sleep(1)}")
            menu_admin()
        else :
            print("Pilihan Salah !!")
            continue
# READ DATA
def cek_data():
    os.system("cls")
    print("""
Pilihan Cek Data 
-----------------   
[1.] Tiket/Rute
[2.] Pemesan
-----------------""")
    casec = input("Masukan Pilihan :  ")
    if casec == '1':
        os.system("cls")
        print()
        tabel_data()
        print(tabel1)
        input('Tekan ENTER untuk ke Menu Admin ----')
        menu_admin()
    elif casec == '2':
        if len(biodata) <= 0:
            print("\n---BELUM ADA YANG MEMESAN---\n")
        else:
            os.system("cls")
            print("DAFTAR NAMA PEMESAN TIKET")
            for i in biodata:
                total = 0
                data = i['data']
                print()
                print("Nama :",i['nama'])
                print("Password :",i['password'])
                if len(data) <= 0:
                    print("Belum memesan\n")
                else:
                    for c in data:
                        angka = c[3]
                        format = "{:,}".format(angka)
                        print(c[0],"-",c[1],c[2],"Tiket", "=",format)
                        total = total + c[3]
                        koma = "{:,}".format(total)
                    print("Total yang harus dibayar adalah: ", f"Rp.{koma}","\n")
                    print("="*50)
        
        input('Tekan ENTER untuk ke Menu Admin ----')
        menu_admin()
#UPDATE DATA
def ubah_data():
    while True:
        os.system("cls")
        from datetime import datetime
        print('''
-------------------    
 [Fitur Ubah Data]
-------------------
 [1.] Ubah Data 
 [2.] kembali ke Menu Admin
-------------------''')
        menu4 = input('Pilih: ')       
        if menu4 == '1':
            os.system("cls")
            tabel_data()
            print(tabel1)
            diUpdate = int(input('Masukkan Nomor Data yang Ingin diUbah : '))-1
            key = diUpdate
            print()
            print()
            while diUpdate in range(len(rute)):
                os.system("cls")
                print("\n\t\t\tTABEL DARI INDEX DATA \n")
                tabel_data_tertentu(key)
                print(tabel3)
                opsi = input('''
 [Apa Yang Ingin Anda Ubah ]
=============================
 [1.] Pelabuhan Asal
 [2.] Pelabuhan Tujuan
 [3.] Tarif per rute
 [4.] Tanggal keberangkatan
 [5.] Jam Keberangkatan
 [6.] kembali ke menu 
=============================
Pilih [1-6] : ''')
                if opsi == '1' :
                    asalbaru =input('Masukan Nama Pelabuhan/Kota Asal Baru : ').upper()
                    while True:
                        confirm = input('Simpan data? (Y/N) : ')
                        if confirm.lower() == 'n':
                            print('\t>>>>>>Data Tidak Ditambahkan<<<<<<')
                            time.sleep(2.5)
                            break
                        elif confirm.lower() == 'y':
                            asalbaru = asalbaru.replace(' ','')
                            rute[diUpdate]['asal'] = asalbaru
                            print("\n\n>>>>>>> Data Terupdate <<<<<<")
                            time.sleep(2.5)
                            break
                        else :
                            print('Error')
                            continue
                elif opsi == '2':
                    tujuanbaru = input('Masukan Kota Tujuan Baru : ')
                    print()
                    while True:
                        confirm = input('Simpan data? (y/n) : ').lower()
                        if confirm == 'n' or confirm=='t':
                            print('\nData Tidak Ditambahkan...')
                            time.sleep(2)
                            break
                        elif confirm == 'y' or confirm == 'iya':
                            tujuanbaru = tujuanbaru.replace(' ','')
                            rute[diUpdate]['tujuan'] = tujuanbaru
                            print('\nData Terupdate.....')
                            time.sleep(2)
                            break
                        else :
                            print('ERROR --> just y or n inputs')
                            continue
                elif opsi == '3':
                    while True:
                        try:
                            tarifbaru = int(input('Masukan Tarif Baru : '))
                            break
                        except ValueError:
                            print("ERROR INPUT!!![Hanya Masukan Angka]")
                            continue
                    while True:    
                        confirm = input('Simpan data? (y/n) : ')
                        if confirm.lower() == 'n':
                            print('Data Tidak Diupdate.......')
                            time.sleep(2.5)
                            break
                        elif confirm.lower() == 'y':
                            rute[diUpdate]['tarif'] = tarifbaru
                            print('\nData Diupdate....')
                            time.sleep(2.5)
                            break
                        else :
                            print('ERROR --> just y or n inputs')
                            continue
                elif opsi == "4":
                    while True:
                        try:
                            inputan0 = input("masukan tanggal (dd-mm-yyyy)contoh (21-08-2002) : ")
                            format_data = datetime.strptime(inputan0,"%d-%m-%Y")
                            tgl = format_data.strftime(f"{'%d-%m-%Y'}")
                            break
                        except ValueError:
                            print("masukan format yg benar")
                            time.sleep(2.5)
                            continue
                    while True:    
                        confirm = input('Simpan data? (y/n) : ')
                        if confirm.lower() == 'n':
                            print('Data Tidak Diupdate.......')
                            time.sleep(2.5)
                            break
                        elif confirm.lower() == 'y':
                            rute[diUpdate]["tanggal"] = tgl                  
                            print('\nData Diupdate....')
                            time.sleep(2.5)
                            break
                        else :
                            print('ERROR --> just y or n inputs')
                            continue
                elif opsi == "5":
                    while True:
                        try:
                            inputan0 = input("Masukan Jam (HH:MM) : ")
                            format_data = datetime.strptime(inputan0,"%H:%M")
                            format_tanggal = format_data.strftime(f"{'%H:%M'}")                            
                            break
                        except ValueError:
                            print("masukan format yg benar")
                            time.sleep(25)                     
                            continue
                    while True:    
                        confirm = input('Simpan data? (y/n) : ')
                        if confirm.lower() == 'n':
                            print('Data Tidak Diupdate.......')
                            time.sleep(2.5)
                            break
                        elif confirm.lower() == 'y':
                            rute[diUpdate]["jam"] = format_tanggal                 
                            print('\nData Diupdate....')
                            time.sleep(2.5)
                            break
                        else :
                            print('ERROR --> just y or n inputs')
                            continue
                elif opsi == '6':
                    time.sleep(1)
                    break
                else:
                    print('Inputan Salah!!!')
                    continue
            else:
                print('Data Tidak ditemukan!!')
                time.sleep(2)

        elif menu4 == '2':
            print("ke Menu Admin....")
            time.sleep(2.5)
            break
        else:
            print("Pilihan Salah !!")
            continue
# DELETE
def hapus_data():
    while True:
        os.system("cls")
        print('''
    Fitur Hapus data
-------------------------
1. Hapus Data 
0. Kembali Ke Menu Utama
-------------------------''')
        menu2 = input("Masukan Pilihan :")
        if menu2 == '1':
            os.system("cls")
            print('\n\t\tBerikut Data Program Tiket Kapal Terkini\n')
            tabel_data()
            print(tabel1)
            while True:
                print("Masukan '0' jika ingin kembali")
                hapus1 = input("Masukkan NO Data yg akan dihapus : ")
                if hapus1 == '0':
                    break
                try:
                    hapus = int(hapus1) - 1
                    if hapus not in range(len(rute)):
                        print('Data Tidak ada...')
                    elif hapus in range(len(rute)):
                        del rute[hapus]                        
                        print('Data terhapus...')
                        time.sleep(2.5)
                        break
                except ValueError:
                    pass
        elif menu2 == '0':
            print("Menuju Ke Menu Admin....")
            time.sleep(2.5)
            break
        else:
            print("Pilihan Salah !!")
            continue
def menulogin():
    while True:
        os.system("cls")
        print("""
  Sistem Pemesanan Tiket Kapal
===============================
        1. Registrasi
        2. Login
        3. Exit
===============================""")
        case = input("Pilih: ")
        if case == '1':
            os.system("cls")
            print("Registrasi")
            nama = input("Masukan nama/username: ")
            if nama in daftarnama or nama == us_admin :
                print("Username tersebut sudah ada\n")
            else:
                pw = input("Masukan Password: ")
                daftarnama.append(nama)
                biodata.append({'nama':nama, 'password':pw, 'data':[]})
                print()    
                print("Berhasil Membuat akun")    
                time.sleep(1.5)
                menulogin()

        elif case == '2':
            nyoba=0
            while nyoba<4:
                os.system('cls')
                print("\t[Menu Login]\n")               
                namas = input("Masukan username: ")
                pws = input("Masukan Password: ")
                for i in biodata:
                    if i['nama'] == namas and i['password'] == pws:
                        data = i['data']
                        print("Berhasil Login\n")
                        print("menuju halaman pemesanan -- ")
                        time.sleep(1.5)
                        menu_user(namas,data)
                    else:
                        pass

                if namas == us_admin  and pws == pw_admin :
                    menu_admin()
                elif len(biodata) == 0:
                        print('anda belum punya akun, silahkan daftar terlebih dahulu')
                        time.sleep(1.5)
                        break
                else:
                    nyoba+=1
                    kesempatan=4
                    print()
                    print("GAGAL LOGIN")
                    print(f"kesempatan login anda : {kesempatan-nyoba} kali lagi")
                    time.sleep(3)
                    continue
                    
    

        elif case == '3':
            exit()
        else :
            print('Pilihan Salah !!')
            time.sleep(1)
            continue 

menulogin()