# from datetime import datetime
import os
import time  #buat time sleep agar ada jeda beberapa detik di program
from prettytable import PrettyTable
os.system('cls')

tabel = PrettyTable(['Asal', 'Tujuan', 'Jumlah Tiket', 'Sub Total (.000)'])
biodata = []
rute = [{
    "asal" : "samarinda",
    "tujuan" : "Balikpapan",
    "tarif" : 100},
    {
    "asal" : "Bogor",
    "tujuan" : "Surabaya",
    "tarif" : 300},

]
   
def tabel_data():
    print("NO | KOTA ASAL \t | KOTA TUJUAN \t | TARIF \t |")
    for i in range(len(rute)):
        print(" {} | {} \t | {} \t | {}.000 \t |" .format(i+1, rute[i]['asal'], rute[i]['tujuan'], rute[i]['tarif']))
        continue

def tabel_data_tertentu(key):
    if key in range(len(rute)):
        print('NO | KOTA ASAL \t | KOTA TUJUAN \t | TARIF \t|')        
        print(' {} | {} \t | {} \t | {}.000\t|' .format(key+1, rute[key]['asal'], rute[key]['tujuan'],rute[key]['tarif']))



user = ['admin']

def registrasi():
    print("Registrasi")
    nama = input("Masukan nama/username: ")
    if nama in user:
        print("Username tersebut sudah ada\n")
    else:
        pw = input("Masukan Password: ")
        user.append(nama)
        biodata.append({'nama':nama, 'password':pw, 'data':[]})
        time.sleep(1)

def cek_login():
    os.system('cls')
    print("Login")
    while True:
        namas = input("Masukan username: ")
        pws = input("Masukan Password: ")
        for i in biodata:
            if i['nama'] == namas and i['password'] == pws:
                data = i['data']
                print("Berhasil Login\n")
                print("Halo",namas,"Selamat datang di pemesanan tiket kapal")
                menu_user(namas, data)
            else:
                pass
        if namas == 'admin' and pws == 'admin':
            menu_admin()
        break

def menu_user(namas, data):
        # masukan = datetime.today()
        # print("Hari",masukan.strftime("%A"))
        # print("jam ",masukan.strftime("%H:%M"))
    while True:
        tabel.clear_rows()
        print("""
1. Lihat Pesanan
2. Pesan tiket
0. Halaman Login
            """)
        casea = input("Pilih: ")
        if casea == '1' or casea == 'lihat':
            user_data(namas, data)
        elif casea == '2' or casea == 'pesan':
            user_pesan(namas, data)
        elif casea == '0' or casea == 'login':
            break

def user_data(namas, data):
    if len(data) <= 0:
        print("\n>>>>>> Anda belum memesan tiket <<<<<<")
    else:
        os.system('cls')
        print("==========================================")
        print("Halo",namas ,"Anda memesan tiket :")
        for i in range(len(data)):
            tabel.add_row([data[i][0],data[i][1],data[i][2],data[i][3]])
        
        print(tabel)

        total = 0
        for i in data:
            total = total + i[3]
        print(f"Total yang harus dibayar adalah: {total}.000")
        print("==========================================")
        print()

def user_pesan(namas, data):
    while True:
        os.system('cls')
        tabel_data()
        while True:
            case1 = input("Ingin pesan tiket apa : ")
            try:
                casebb = int(case1)-1
                if case1 == '0':
                    menu_user(namas, data)
                break
            except ValueError:
                continue
        # for i in range(len(rute)):
        if casebb in range(len(rute)):
            while True:
                berapa = input("Ingin pesan berapa tiket ? : ")
                try:
                    berapa1 = int(berapa)
                    break
                except ValueError:
                    continue
            print(f'\nAnda Memilih Rute {rute[casebb]["asal"]} - {rute[casebb]["tujuan"]} Dengan Harga Rp{rute[casebb]["tarif"]}.000 x {berapa} orang')
            totals = rute[casebb]['tarif'] * berapa1
            data.append([rute[casebb]['asal'],rute[casebb]['tujuan'],berapa1, totals])
            tanya = input("\nApakah Anda Ingin Memesan Lagi ? (y/n) : ")
            if tanya.lower() == 'y':
                os.system('cls')
                continue

            else :
                print()
                print('>>>>>>>>>>> Terimakasih Telah Memesan <<<<<<<<<<')
                print()
                break
        else:
            continue
               

def menu_admin():
    while True:
        print(">>>>>>>>>>> Selamat Datang Admin <<<<<<<<<<")
        print("""
1. Tambah Data
2. Cek Data
3. Ubah Data
4. Hapus Data
0. Halaman Login
        """)
        caseb = input("Pilih: ")
        if caseb == '1':
            tambah_data()
        elif caseb == '2':
            cek_data()
        elif caseb == '3':
            ubah_data()
        elif caseb == '4':
            hapus_data()
        elif caseb == '0':
            break
# Create
def tambah_data():
    while True:
        menu1 = (input('''
Tambah data :
1. menambah rute dan tarif
2. kembali ke Menu Utama
Pilih : '''))
        if menu1 == '1':
            while True:
                asal = input("Tambahkan Asal Keberangkatan : ")
                tujuan = input("Tambahkan Tujuan Keberangkatan : ")
                tarif = input("Tambahkan Tarif Rute : ")
                try:
                    tarif = int(tarif)
                    rute.append({'asal':asal,'tujuan':tujuan,'tarif':tarif})
                    tabel_data()
                    break
                except ValueError:
                    print("TARIF HARUS BERUPA ANGKA!")
        elif menu1 == '2' :
            os.system('cls')
            break
        else :
            print("Pilihan Salah !!")
            continue

# Read
def cek_data():
    print("""
1. Tiket/Rute
2. Pemesan
    """)
    casec = input("Ingin mengecek data apa? ")
    if casec == '1':
        tabel_data()
        time.sleep(2.5)
    elif casec == '2':
        if len(biodata) <= 0:
            print("Belum ada pemesan\n")
        else:
            print("Berikut adalah nama2 orang yang memesan tiket")
            for i in biodata:
                total = 0
                data = i['data']
                print()
                print("Nama :",i['nama'])
                print("Password :",i['password'])
                if len(data) <= 0:
                    print("Belum memesan\n")
                else:
                    for c in data:
                        print(c[0],"-",c[1],c[2],"Tiket", "=",c[3])
                        total = total + c[3]
                    print("Total yang harus dibayar adalah: ", total,"\n")
                    print("="*20)
        time.sleep(2)

# update
def ubah_data():
    while True:
        menu4 = input('''
Fitur Update Data
1. Ubah Data 
2. kembali ke Menu
Pilih: ''')       
        if menu4 == '1':
            os.system("cls")
            tabel_data()
            diUpdate = int(input('Masukkan Nomor Data yang Ingin diUbah : '))-1
            key = diUpdate
            print()
            print()
            while diUpdate in range(len(rute)):
                tabel_data_tertentu(key)
                opsi = input('''
Apa Yang Ingin Anda Ubah :
1. Kota Asal
2. Kota Tujuan
3. Tarif
4. kembali ke menu
Pilih: ''')
                if opsi == '1' :
                    asalbaru =input('Masukan Kota Asal Baru : ')
                    confirm = input('Simpan data? (Y/N) : ')
                    if confirm.lower() == 'n':
                        print('Data Tidak Ditambahkan')
                        break
                    elif confirm.lower() == 'y':
                        asalbaru = asalbaru.replace(' ','')
                        rute[diUpdate]['asal'] = asalbaru
                        
                        print("\n\n>>>>>>> Data Terupdate <<<<<<")
                    else :
                        print('Error')
                        continue
                elif opsi == '2':
                    tujuanbaru = input('Masukan Kota Tujuan Baru : ')
                    confirm = input('Simpan data? (Y/N) : ')
                    if confirm.lower() == 'n':
                        print('Data Tidak Ditambahkan')
                        break
                    elif confirm.lower() == 'y':
                        tujuanbaru = tujuanbaru.replace(' ','')
                        rute[diUpdate]['tujuan'] = tujuanbaru

                        print('Data Terupdate')
                    else :
                        print('Error')
                        continue
                elif opsi == '3':
                    while True:
                        tarifbaru = input('Masukan Tarif Baru : ')
                        confirm = input('Simpan data? (Y/N) : ')
                        if confirm.lower() == 'n':
                            print('Data Tidak Diupdate.......')
                            break
                        elif confirm.lower() == 'y':
                            tarifbaru = tarifbaru.replace(' ','')
                            try:
                                rute[diUpdate]['tarif'] = int(tarifbaru)
                                print('\nData Diupdate....')
                                break
                            except ValueError:
                                print("GAGAL")
                                break
                        else :
                            print('Error')
                            continue
                elif opsi == '4':
                    break
                else:
                    print('Inputan Salah!!!')
                    continue
            else:
                print('Data Tidak ditemukan!!')
        elif menu4 == '2':
            os.system('cls')
            break
        else:
            print("Pilihan Salah !!")
            continue


# Delete
def hapus_data():
    while True:
        menu2 = input('''
Fitur Hapus data
1. Hapus Data 
2. Kembali Ke Menu Utama
Pilih: ''')

        if menu2 == '1':
            os.system("cls")
            print('Berikut Data Rute dan Tarif Bus Saat ini, Silahkan Hapus Sesuai Nomor.')
            tabel_data()
            while True:
                hapus = input('Masukkan Nomor yang akan dihapus : ')
                try:
                    hapus = int(hapus) - 1
                    if hapus not in range(len(rute)):
                        print('Data Tidak ada...')
                        continue
                    elif hapus in range(len(rute)):
                        del rute[hapus]
                        
                        print('Data terhapus...')
                        break
                except ValueError:
                    pass
        elif menu2 == '2':
            os.system('cls')
            break
        else:
            print("Pilihan Salah !!")
            continue

while True:
    print('Sistem Pemesanan Tiket Kapal')
    print("""
=============================
1. Registrasi
2. Login
3. Stop
=============================
    """)
    case = input("Pilih: ")
    if case == '1':
        registrasi()
    elif case == '2':
        cek_login()
    elif case == '3':
        exit()
    else :
        print('Pilihan Salah !!')
        time.sleep(1)
        continue 